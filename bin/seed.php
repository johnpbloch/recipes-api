#!/usr/bin/env php
<?php

require __DIR__ . '/../bootstrap.php';

use League\FactoryMuffin\Facade as Muffin;

$c = new League\CLImate\CLImate;
$c->forceAnsiOn();

$c->info('Setting up factories...');

require_once CONFIG . '/muffins.php';

$c->info('Cleaning up old data and seeding the database...');

$progress = $c->progress(15707);

$advance = function () use ($progress) {
    $progress->advance();
};
foreach ([
             'seed:johnpbloch\recipesApi\Models\Recipe',
             'seed:johnpbloch\recipesApi\Models\IngredientGroup',
             'seed:johnpbloch\recipesApi\Models\Ingredient',
             'seed:johnpbloch\recipesApi\Models\Category',
             'seed:johnpbloch\recipesApi\Models\RecipeCategory',
             'seed:johnpbloch\recipesApi\Models\Note',
             'seed:johnpbloch\recipesApi\Models\User',
         ] as $class) {
    Muffin::define($class, [], $advance);
}

(new \johnpbloch\recipesApi\Models\RecipeCategoryQuery())->deleteAll();
$progress->advance();
(new \johnpbloch\recipesApi\Models\CategoryQuery())->deleteAll();
$progress->advance();
(new \johnpbloch\recipesApi\Models\IngredientQuery())->deleteAll();
$progress->advance();
(new \johnpbloch\recipesApi\Models\IngredientGroupQuery())->deleteAll();
$progress->advance();
(new \johnpbloch\recipesApi\Models\NoteQuery())->deleteAll();
$progress->advance();
(new \johnpbloch\recipesApi\Models\RecipeQuery())->deleteAll();
$progress->advance();
(new \johnpbloch\recipesApi\Models\UserQuery())->deleteAll();
$progress->advance();

$getRandom = function (array $things) {
    $count = count($things) - 1;
    return array_values($things)[rand(0, $count)];
};

$users = Muffin::seed(50, 'johnpbloch\recipesApi\Models\User');
$categories = Muffin::seed(150, 'johnpbloch\recipesApi\Models\Category');
$recipes = [];
foreach ($users as $user) {
    /** @var johnpbloch\recipesApi\Models\User $user */
    $recipeCount = rand(10, 20);
    $userRecipes = Muffin::seed(
        $recipeCount,
        'johnpbloch\recipesApi\Models\Recipe',
        ['UserId' => $user->getPrimaryKey()]
    );
    $doubleGroups = $getRandom($userRecipes);
    foreach ($userRecipes as $recipe) {
        /** @var johnpbloch\recipesApi\Models\Recipe $recipe */
        $primaryGroup = Muffin::create(
            'johnpbloch\recipesApi\Models\IngredientGroup',
            ['RecipeId' => $recipe->getPrimaryKey()]
        );
        $secondaryGroup = null;
        $hasTwoGroups = $doubleGroups === $recipe;
        if ($hasTwoGroups) {
            $secondaryGroup = Muffin::create(
                'johnpbloch\recipesApi\Models\IngredientGroup',
                ['RecipeId' => $recipe->getPrimaryKey()]
            );
        }
        $categoryCount = rand(1, 3);
        $recipeCategories = [];
        for ($x = 0; $x < $categoryCount; $x += 1) {
            /** @var johnpbloch\recipesApi\Models\RecipeCategory $category */
            $category = $getRandom($categories);
            if (!in_array($category, $recipeCategories)) {
                $recipeCategories[] = $category;
                Muffin::create(
                    'johnpbloch\recipesApi\Models\RecipeCategory',
                    [
                        'RecipeId' => $recipe->getPrimaryKey(),
                        'CategoryId' => $category->getPrimaryKey(),
                    ]
                );
            }
        }
        $progress->advance(3 - count($recipeCategories));
        $maximumIngredients = 10;
        $ingredientCount = $primaryLimit = rand(5, $maximumIngredients);
        if ($hasTwoGroups) {
            $maximumIngredients = 18;
            $ingredientCount = ceil($ingredientCount * 1.8);
            $primaryLimit = ceil($ingredientCount / 2);
        }
        $group = $primaryGroup;
        for ($x = 0; $x < $ingredientCount; $x += 1) {
            if ($hasTwoGroups && $primaryLimit === $x) {
                $group = $secondaryGroup;
            }
            /** @var johnpbloch\recipesApi\Models\IngredientGroup $group */
            Muffin::create(
                'johnpbloch\recipesApi\Models\Ingredient',
                [
                    'GroupId' => $group->getPrimaryKey(),
                    'RecipeId' => $recipe->getPrimaryKey(),
                ]
            );
        }
        $progress->advance($maximumIngredients - $ingredientCount);
    }
    $progress->advance(20 - $recipeCount);
    $recipes = array_merge($recipes, $userRecipes);
}

for ($x = 0; $x < 50; $x += 1) {
    Muffin::create(
        'johnpbloch\recipesApi\Models\Note',
        [
            'RecipeId' => $getRandom($recipes)->getPrimaryKey(),
            'UserId' => $getRandom($users)->getPrimaryKey(),
        ]
    );
}

$c->info('All done!');
