<?php

use johnpbloch\recipesApi\Application;
use League\Route\Http\Exception\NotFoundException;
use League\Route\RouteCollection;
use League\Route\Strategy\RestfulStrategy;
use Symfony\Component\HttpFoundation\Request;

call_user_func(function (Application $app, RouteCollection $router) {
    $router->setStrategy(new RestfulStrategy());

    // Use a closure to register routes with JIT autoloading of controllers
    $getCb = function (...$callback) use ($app) {
        return function (Request $request, array $args = []) use (
            $callback,
            $app
        ) {
            if (!is_object($callback[0]) && !class_exists($callback[0])) {
                $callback[0] = $app->get($callback[0]);
            }
            if (1 === count($callback)) {
                $callback = $callback[0];
            }
            if (!is_callable($callback)) {
                return new NotFoundException();
            }
            return $callback($request, ...$args);
        };
    };

    // Time to register some routes!
    // First let's store the controllers' DI aliases
    $recipes = 'recipesController';
    $ingredients = 'ingredientsController';
    $categories = 'categoriesController';
    $users = 'usersController';

    // Now the recipes routes
    $router
        ->get('/recipes', $getCb($recipes, 'getList'))
        ->post('/recipes', $getCb($recipes, 'create'))
        ->get('/recipes/{id}', $getCb($recipes, 'show'))
        ->put('/recipes/{id}', $getCb($recipes, 'update'))
        ->delete('/recipes/{id}', $getCb($recipes, 'delete'))
        ->get('/recipes/{id}/notes', $getCb($recipes, 'getNotes'))
        ->post('/recipes/{id}/notes', $getCb($recipes, 'addNote'))
        ->get('/recipes/{id}/ingredients', $getCb($ingredients, 'index'))
        ->get('/recipes/{id}/groups', $getCb($recipes, 'getGroups'))
        ->post('/recipes/{id}/groups', $getCb($recipes, 'addGroup'));

    // Categories routes next
    $router
        ->get('/categories', $getCb($categories, 'getList'))
        ->post('/categories', $getCb($categories, 'create'));

    // Ingredients routes after that
    $router
        ->get('/ingredients', $getCb($ingredients, 'getList'))
        ->post('/ingredients', $getCb($ingredients, 'create'));

    // Finally the users routes
    $router
        ->get('/users', $getCb($users, 'getList'))
        ->post('/users', $getCb($users, 'create'))
        ->get('/users/{id}', $getCb($users, 'show'))
        ->put('/users/{id}', $getCb($users, 'update'))
        ->delete('/users/{id}', $getCb($users, 'delete'))
        ->get('/users/{id}/recipes', $getCb($recipes, 'userIndex'));

}, $app, $app['router']);
