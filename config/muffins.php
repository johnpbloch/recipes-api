<?php

use League\FactoryMuffin\Facade as Muffin;

Muffin::setCustomSetter(function ($object, $field, $value) {
    $object->setByName($field, $value);
});

Muffin::define('johnpbloch\recipesApi\Models\Recipe', [
    'Id' => 'uuid',
    'Name' => 'sentence|3',
    'Instructions' => 'text',
    'UserId' => 'factory|johnpbloch\recipesApi\Models\User',
]);

Muffin::define('johnpbloch\recipesApi\Models\IngredientGroup', [
    'Id' => 'uuid',
    'RecipeId' => 'factory|johnpbloch\recipesApi\Models\Recipe',
    'Name' => 'optional:word',
]);

Muffin::define('johnpbloch\recipesApi\Models\Ingredient', [
    'Id' => 'uuid',
    'Name' => 'word',
    'RecipeId' => 'factory|johnpbloch\recipesApi\Models\Recipe',
    'GroupId' => 'factory|johnpbloch\recipesApi\Models\IngredientGroup',
    'Quantity' => 'optional:regexify|\d (C|T|t)',
]);

Muffin::define('johnpbloch\recipesApi\Models\Category', [
    'Id' => 'uuid',
    'Name' => 'unique:word',
]);

Muffin::define('johnpbloch\recipesApi\Models\RecipeCategory', [
    'RecipeId' => 'factory|johnpbloch\recipesApi\Models\Recipe',
    'CategoryId' => 'factory|johnpbloch\recipesApi\Models\Category',
]);

Muffin::define('johnpbloch\recipesApi\Models\Note', [
    'Id' => 'uuid',
    'Body' => 'text',
    'RecipeId' => 'factory|johnpbloch\recipesApi\Models\Recipe',
    'UserId' => 'factory|johnpbloch\recipesApi\Models\User',
]);

Muffin::define('johnpbloch\recipesApi\Models\User', [
    'Id' => 'uuid',
    'Email' => 'unique:safeEmail',
    'Name' => 'optional:name',
]);
