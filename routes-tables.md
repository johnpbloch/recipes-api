### Recipes Route Table

| Action | Endpoint | Route |
| :----- | :------- | :---- |
| Create | POST /recipes | `$router->post('recipes', [$recipesController, 'create']);` |
| Read | GET /recipes/{id} | `$router->get('recipes/{id}', [$recipesController, 'show']);` |
| Update | PUT /recipes/{id} | `$router->put('recipes/{id}', [$recipesController, 'update']);` |
| Delete | DELETE /recipes/{id} | `$router->delete('recipes/{id}', [$recipesController, 'delete']);` |
| List | GET /recipes | `$router->get('recipes', [$recipesController, 'list']);` |
| Get Notes | GET /recipes/{id}/notes | `$router->get('recipes/{id}/notes', [$recipesController, 'getNotes']);` |
| Add Note | POST /recipes/{id}/notes | `$router->post('recipes/{id}/notes', [$recipesController, 'addNote']);` |
| Ingredients | GET /recipes/{id}/ingredients | `$router->post('recipes/{id}/ingredients', [$ingredientsController, 'index']);` |
| Get Ingredients Groups | GET /recipes/{id}/groups | `$router->get('recipes/{id}/groups', [$recipesController, 'getGroups']);` |
| Add Ingredients Group | POST /recipes/{id}/groups | `$router->post('recipes/{id}/groups', [$recipesController, 'addGroup']);` |

### Categories Route Table

| Action | Endpoint | Route |
| :----- | :------- | :---- |
| Create | POST /categories | `$router->post('categories', [$categoriesController, 'create']);` |
| List | GET /categories | `$router->get('categories', [$categoriesController, 'list']);` |

### Ingredients Route Table

| Action | Endpoint | Route |
| :----- | :------- | :---- |
| Create | POST /ingredients | `$router->post('ingredients', [$ingredientsController, 'create']);` |
| List | GET /ingredients | `$router->get('ingredients', [$ingredientsController, 'list']);` |

### Users Route Table

| Action | Endpoint | Route |
| :----- | :------- | :---- |
| Create | POST /users | `$router->post('users', [$usersController, 'create']);` |
| Read | GET /users/{id} | `$router->get('users/{id}', [$usersController, 'show']);` |
| Update | PUT /users/{id} | `$router->put('users/{id}', [$usersController, 'update']);` |
| Delete | DELETE /users/{id} | `$router->delete('users/{id}', [$usersController, 'delete']);` |
| List | GET /users | `$router->get('users', [$usersController, 'list']);` |
| Recipes | GET /users/{id}/recipes | `$router->get('users/{id}/recipes', [$recipesController, 'userIndex']);` |
