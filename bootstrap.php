<?php

use johnpbloch\recipesApi\Application;
use johnpbloch\recipesApi\Controllers\Categories;
use johnpbloch\recipesApi\Controllers\Ingredients;
use johnpbloch\recipesApi\Controllers\Recipes;
use johnpbloch\recipesApi\Controllers\Users;
use League\Route\RouteCollection;

const ROOT = __DIR__;
const CONFIG = ROOT . '/config';

require_once ROOT . '/vendor/autoload.php';

require_once ROOT . '/generated-conf/config.php';

if (!isset($app) || !($app instanceof Application)) {
    $app = new Application();
}

$app->add('router', function () {
    return new RouteCollection();
}, true);

$app->add('recipesController', function () {
    return new Recipes();
});

$app->add('categoriesController', function () {
    return new Categories();
});

$app->add('ingredientsController', function () {
    return new Ingredients();
});

$app->add('usersController', function () {
    return new Users();
});
