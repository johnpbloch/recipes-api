<?php

namespace johnpbloch\recipesApi\Controllers;

use Symfony\Component\HttpFoundation\Request;

class Users extends Base
{

    public function getList(Request $request)
    {
    }

    public function create(Request $request)
    {
    }

    public function show(Request $request, $id)
    {
    }

    public function update(Request $request, $id)
    {
    }

    public function delete(Request $request, $id)
    {
    }
}
