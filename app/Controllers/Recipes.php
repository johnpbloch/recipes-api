<?php

namespace johnpbloch\recipesApi\Controllers;

use Symfony\Component\HttpFoundation\Request;

class Recipes extends Base
{

    public function getList(Request $request)
    {
    }

    public function create(Request $request)
    {
    }

    public function show(Request $request, $id)
    {
    }

    public function update(Request $request, $id)
    {
    }

    public function delete(Request $request, $id)
    {
    }

    public function getNotes(Request $request, $id)
    {
    }

    public function addNote(Request $request, $id)
    {
    }

    public function getGroups(Request $request, $id)
    {
    }

    public function addGroup(Request $request, $id)
    {
    }

    public function userIndex(Request $request, $userId)
    {
    }
}
