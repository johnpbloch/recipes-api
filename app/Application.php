<?php

namespace johnpbloch\recipesApi;

use Exception;
use League\Container\Container;
use League\Route\Http\Exception as HttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use League\Route\RouteCollection;

class Application extends Container implements HttpKernelInterface
{

    /**
     * Handles a Request to convert it to a Response.
     *
     * When $catch is true, the implementation must catch all exceptions
     * and do its best to convert them to a Response instance.
     *
     * @param Request $request A Request instance
     * @param int $type The type of the request
     *                         (one of HttpKernelInterface::MASTER_REQUEST or HttpKernelInterface::SUB_REQUEST)
     * @param bool $catch Whether to catch exceptions or not
     *
     * @return Response A Response instance
     *
     * @throws Exception When an Exception occurs during processing
     *
     * @api
     */
    public function handle(
        Request $request,
        $type = self::MASTER_REQUEST,
        $catch = true
    ) {
        /** @var RouteCollection $router */
        $router = $this->get('router');
        return $catch
            ? $this->safeGetResponse($request, $router)
            : $this->getResponse($request, $router);
    }

    /**
     * @param Request $request
     * @param RouteCollection $router
     * @return Response
     */
    protected function safeGetResponse(
        Request $request,
        RouteCollection $router
    ) {
        try {
            $response = $this->getResponse($request, $router);
        } catch (HttpException $e) {
            $response = new Response($e->getMessage(), $e->getStatusCode());
        } catch (Exception $e) {
            $response = new Response($e->getMessage(), 500);
        }
        return $response;
    }

    /**
     * @param Request $request
     * @param RouteCollection $router
     * @return Response
     */
    protected function getResponse(Request $request, RouteCollection $router)
    {
        return $router
            ->getDispatcher()
            ->dispatch(
                $request->getMethod(),
                rtrim($request->getPathInfo(), '/')
            );
    }
}
