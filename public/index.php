<?php

use Symfony\Component\HttpFoundation\Request;

require __DIR__ . '/../bootstrap.php';
require_once CONFIG . '/routes.php';

$request = Request::createFromGlobals();

$app = (new \Stack\Builder())
    ->resolve($app);

$app->terminate($request, $app->handle($request)->send());
